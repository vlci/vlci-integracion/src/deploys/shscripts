#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

function setArguments() {
    # Environment
    case $1 in
    PRE)
        SFTP_ENV=pre
        GIT_BRANCH=test
        ;;
    PRO)
        SFTP_ENV=prod
        GIT_BRANCH=main
        ;;
    *)
        echo "Uso: $ deploy_scripts.sh (PRO|PRE) {TAGNAME} {COMMENT}" 1>&2
        echo "Example: $ deploy_scripts.sh PRO 2023_S09 Sprint_2023_S09" 1>&2
        exit 2
        ;;
    esac
    ENVIRONMENT=$1

    # Global Variables
    SCRIPTS_PATH=/opt/etls/sc_vlci/${ENVIRONMENT}/shscripts/postgis-sh
    SH_PATH=/opt/etls/sc_vlci/${ENVIRONMENT}
    SH_GIT_REPO=git@gitlab.com:vlci/vlci-integracion/src/shell-scripts/postgis-sh.git
    SH_GIT_REPO_NAME=postgis-sh
}

function get_code() {
    rm -rf ${SCRIPTS_PATH}
    git clone -b ${GIT_BRANCH} ${SH_GIT_REPO} ${SCRIPTS_PATH}

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function get_code: ShellScripts refreshed from the gitlab repository"
}

function save_secret_code() {
    mv ${SCRIPTS_PATH}/secrets.${ENVIRONMENT}.sh /tmp/.

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function save_secret_code: Save config variables aside"
}

function get_secret_code() {
    mv /tmp/secrets.${ENVIRONMENT}.sh ${SCRIPTS_PATH}/. 

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function get_secret_code: Restore config variables"
}

function configure() {
    ln -s ${SCRIPTS_PATH}/secrets.${ENVIRONMENT}.sh ${SCRIPTS_PATH}/secrets.sh

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function configure: Secrets symbolic link created"
}


function mergeTestMainANDTag() {
    if [ "$ENVIRONMENT" = "PRO" ]; then
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        TAGNAME=$1
        COMMENT=$2

        cd /tmp
        git clone ${SH_GIT_REPO}
        cd ${SH_GIT_REPO_NAME}

        # Merge test to main
        git switch main
        git pull
        echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMainANDTag: Main branch refreshed locally"

        git switch test
        git pull
        echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMainANDTag: Test branch refreshed locally"

        git switch main
        git merge test  -m "${COMMENT}" --log --no-edit
        echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMainANDTag: Merged test changes to main"

        git push
        echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMainANDTag: Pushed main branch"
        
        # Tag
        if [ $(git tag -l "${TAGNAME}") ]; then
            LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
            echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMainANDTag: Tag already exists, avoid creating it"
        else
            LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
            git switch main
            echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMainANDTag: Test branch refreshed locally"

            git tag -a ${TAGNAME} -m "${COMMENT}"
            git push origin ${TAGNAME}
            echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMainANDTag: Pushed tag in main branch"
        fi    

        rm -rf ../${SH_GIT_REPO_NAME}
    fi
}

############################################################
# MAIN PROGRAM
############################################################
if [ "$#" -ne 3 ]; then
    echo "Uso: $ deploy_scripts.sh (PRO|PRE) {TAGNAME} {COMMENT}"
    echo "Example: $ deploy_scripts.sh PRO 2023_S09 Sprint_2023_S09"
    exit 2
fi

setArguments $1

save_secret_code
get_code
get_secret_code
configure
mergeTestMainANDTag $2 $3

exit ${RESULT}